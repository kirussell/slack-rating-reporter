# README #

### What is this repository for? ###
* Track your Google Play and App Store rating in Slack
* Version 0.2

### How do I get set up? ###
* Clone this repo
* Just run ```python app_rating.py```
* Dir rating_files must be writable

### TODO ###
* Config file for user with slack token and channel
* setup.py must install app as cli programm
* Auto init crontab job
* Unit tests

### Contribution guidelines ###
* Writing tests
* Code review
* Other guidelines
