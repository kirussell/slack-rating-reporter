# coding=utf-8
import sys

from parsers.app_store import AppStoreParser
from parsers.google_play import GooglePlayParser
from slack_helper import SlackClient
from rating_storage import RatingStorage


MESSAGE_AVG = 'App average rating: %(rating).6f %(heart)s'
MESSAGE_CUR = 'App current version rating: %(cur_rating).6f %(cur_heart)s'
MESSAGE_CHART = 'App average rating: <%(chart)s|%(rating).6f> %(heart)s'

CHANNEL = "#random"

SLACK_URL='YOUR_SLACK_URL'


def _get_status_emoji(rating_diff):
    if rating_diff is None or rating_diff == 0:
        return ""
    if rating_diff > 0:
        return ":green_heart:"
    elif rating_diff < 0:
        return ":broken_heart:"


def send_appstore(rating_folder, slack):
    parser = AppStoreParser(APP_STORE_ID)
    storage = RatingStorage(rating_folder, parser.__class__.__name__)
    storage.save_rating(parser.get_avg_rating(), parser.get_cur_rating())
    chart = storage.get_avg_rating_chart()
    if chart:
        message = MESSAGE_CHART + "\n" + MESSAGE_CUR
    else:
        message = MESSAGE_AVG + "\n" + MESSAGE_CUR

    # Отправялем сообщение Appstore
    print slack.send_message(
        CHANNEL,
        message % {
            "rating": storage.get_newest_avg_rating(),
            "heart": _get_status_emoji(storage.get_newest_avg_rating_diff()),
            "cur_rating": storage.get_newest_cur_rating(),
            "cur_heart": _get_status_emoji(storage.get_newest_cur_rating_diff()),
            "chart": chart
        },
        'App Store',
        'http://b.cdn.tf/appstore_logo.png'
    )


def send_google_play(rating_folder, slack):
    parser = GooglePlayParser("YOU_APP_PACKAGE")
    storage = RatingStorage(rating_folder, parser.__class__.__name__)
    # Сохраняем текущий рейтинг
    storage.save_rating(parser.get_avg_rating(), parser.get_cur_rating())
    chart = storage.get_avg_rating_chart()
    if chart:
        message = MESSAGE_CHART
    else:
        message = MESSAGE_AVG

    # Отправялем сообщение Google Play
    print slack.send_message(
        CHANNEL,
        message % {
            "rating": storage.get_newest_avg_rating(),
            "heart": _get_status_emoji(storage.get_newest_avg_rating_diff()),
            "chart": chart
        },
        'Google Play',
        'http://b.cdn.tf/google_play_logo.png'
    )
    return slack


def send_rating():
    if len(sys.argv) < 2:
        print >>sys.stderr, "Usage: topface_rating.py /RATING/FILES/PATH"
        return

    rating_files = sys.argv[1]
    slack = SlackClient(SLACK_URL)
    send_google_play(rating_files, slack)
    send_appstore(rating_files, slack)


if __name__ == "__main__":
    send_rating()
