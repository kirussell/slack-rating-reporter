# coding=utf-8
import json

from grab import Grab


class AppStoreParser:
    __result = {}

    RATING_PAGE_URL = 'https://api.appannie.com/v1.1/apps/ios/app/%s/ratings'
    HEADERS = {"Authorization": "bearer YOU_TOKEN", "Accept": "application/json"}
    __rate_weight = {1: 'star_1_count', 2: 'star_2_count', 3: 'star_3_count', 4: 'star_4_count', 5: 'star_5_count'}

    def __init__(self, app_id):
        self.app_id = app_id

    def load_rating(self):
        grab = Grab()
        grab.setup(headers=self.HEADERS)
        grab.go(self.RATING_PAGE_URL % self.app_id)

        data = json.loads(grab.response.body)

        self._count_rating(data)

    def get_avg_rating(self):
        if not self.__result:
            self.load_rating()

        return self.__result['rating_avg']

    def get_cur_rating(self):
        if not self.__result:
            self.load_rating()

        return self.__result['rating_cur']

    def _count_rating(self, data):
        cur_rating_sum = 0
        avg_rating_sum = 0

        cur_rating_cnt_overall = 0
        avg_rating_cnt_overall = 0

        for item in data['rating_list']:

            for weight in self.__rate_weight:
                rate_key = self.__rate_weight[weight]

                # Получаем число оценок
                cur_rating_cnt = item['current_ratings'][rate_key]
                avg_rating_cnt = item['all_ratings'][rate_key]

                if cur_rating_cnt:
                    # Добавляем к общему числу оценок
                    cur_rating_cnt_overall += int(cur_rating_cnt)
                    # Умножаем число оценок на их значение (т.е. для 5-к на 5, 2-к на 2 и т.д)
                    cur_rating_sum += int(cur_rating_cnt) * weight

                if avg_rating_cnt:
                    avg_rating_cnt_overall += int(avg_rating_cnt)
                    avg_rating_sum += int(avg_rating_cnt) * weight

        if cur_rating_sum > 0:
            self.__result['rating_cur'] = float(cur_rating_sum) / cur_rating_cnt_overall

        if avg_rating_sum > 0:
            self.__result['rating_avg'] = float(avg_rating_sum) / avg_rating_cnt_overall
