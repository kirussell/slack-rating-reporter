# coding=utf-8
from grab import Grab


class GooglePlayParser:
    def __init__(self, package):
        self.package = package

    __rate_classes = {1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five'}

    __result = {}

    @staticmethod
    def _create_xpath_for(rating_value):
        return '//div[@class="rating-bar-container %s"]//span[@class="bar-number"]' % rating_value

    def _create_gp_url(self):
        return 'https://play.google.com/store/apps/details?id=%s' % self.package

    def load_rating(self):
        """
        Загружает оценки из Google Play
        """

        g = Grab()
        g.go(self._create_gp_url())

        for num in self.__rate_classes:
            xpath = self._create_xpath_for(self.__rate_classes[num])
            self.__result[num] = g.doc.select(xpath).text().replace(" ", "")

    def _count_avg_rate(self):
        rate_sum = 0
        rate_count = 0
        for rating in self.__rate_classes:
            value = int(self.__result[rating])
            rate_sum += value * rating
            rate_count += value

        return float(rate_sum) / float(rate_count)

    def get_avg_rating(self):
        """
        Если рейтиг не загружен, то грузит его и считает значения общего рейтинга приложения
        :return: общий рейтинг всех версий приложения
        :rtype: float
        """
        if not self.__result:
            # Загружаем рейтинг
            self.load_rating()

        return self._count_avg_rate()

    # noinspection PyMethodMayBeStatic
    def get_cur_rating(self):
        """
        Рейтинг текущей версии приложения не поддерживается для Google Play
        :return: Рейтинг текущей версии приложения
        :rtype: float
        """
        return None