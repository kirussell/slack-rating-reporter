# coding=utf-8
import cPickle
import time


class RatingStorage:
    """
    Класс предоставляющий возможность сохранять и получать данные о рейтинге
    """

    # Количество измерений,ы выводимое на графике
    CHART_MAX_RATINGS = 15

    # Количесто хранимых в файле запросов рейтинга
    RATING_SAVE_COUNT = 30

    # Ключ для хранения рейтинга текущей версии приложения
    CURRENT_RATING_KEY = 'rating_cur'

    # Ключ для хранения общего рейтинга приложения
    AVERAGE_RATING_KEY = 'rating_avg'

    # Ключ для хранения даты записи рейтинга
    DATE_KEY = "date"

    # Ключ для хранения текущей версии формата данных
    VERSION_KEY = "ver"

    # Текущая версия формата данных
    VERSION = 1

    # URL графика рейтингов
    CHART_URL = "https://chart.googleapis.com/chart?" \
                "cht=ls&" \
                "chs=200x75&" \
                "chd=t:%(data)s&" \
                "chds=%(axis_start)f,%(axis_end)f&" \
                "chxr=0,%(axis_start)f,%(axis_end)f,0.001&" \
                "chxt=y" \
                "&chxs=0N*3zs"

    __rating = []

    def __init__(self, rating_path, rating_name):
        self.__pkl_file = rating_path + "/rating_" + rating_name + ".p"

    def save_rating(self, rating_avg, rating_cur):
        cur_rating = self.get_rating()
        if not cur_rating:
            cur_rating = []

        storage_file = open(self.__pkl_file, "w+b")

        # Добавляем данные о рейтинге в начало списка
        cur_rating[:0] = [{
            self.VERSION_KEY: self.VERSION,
            self.DATE_KEY: time.time(),
            self.AVERAGE_RATING_KEY: rating_avg,
            self.CURRENT_RATING_KEY: rating_cur
        }]

        cPickle.dump(cur_rating[0:self.RATING_SAVE_COUNT], storage_file)
        storage_file.close()

    def get_rating(self, need_reload=False):
        """
        :param need_reload: если True, то рейтинг будет перечит из файла
        :return: Возвращает dictionary с данными последнего запроса рейтинга
        :rtype: dict
        """
        # Если рейтинг пустой или необходимо его принудительно перечетать, то пробуем его получить из файла
        if need_reload or not self.__rating:
            try:
                storage_file = open(self.__pkl_file, "r+b")
                self.__rating = cPickle.load(storage_file)
                storage_file.close()
            except EOFError:
                print "Rating file is empty"
            except IOError:
                print "Can't load rating file"

        return self.__rating

    def get_newest_avg_rating(self):
        """
        :return: float общий рейтинг всех версий
        """
        rating = self.get_rating()
        if len(rating) > 0:
            return rating[0][self.AVERAGE_RATING_KEY]
        else:
            return 0.0

    def get_newest_cur_rating(self):
        """
        :return: float рейтинг текущей версии приложения
        """
        rating = self.get_rating()
        if len(rating) > 0:
            return rating[0][self.CURRENT_RATING_KEY]
        else:
            return 0.0

    def get_newest_avg_rating_diff(self):
        """
        :return: разницу в последних двух запросах рейтинга
                (т.е. если рейтинг вырос то положительное число, если упал, то отрицательное)
        :rtype: float
        """
        rating = self.get_rating()
        if len(rating) > 1:
            return rating[0][self.AVERAGE_RATING_KEY] - rating[1][self.AVERAGE_RATING_KEY]
        else:
            return 0.0

    def get_newest_cur_rating_diff(self):
        """
        :return: разницу в последних двух запросах рейтинга текущей версии приложения
                (т.е. если рейтинг вырос то положительное число, если упал, то отрицательное)
        :rtype: float
        """
        rating = self.get_rating()
        if len(rating) > 1 \
                and rating[0][self.CURRENT_RATING_KEY] is not None \
                and rating[1][self.CURRENT_RATING_KEY] is not None:

            return rating[0][self.CURRENT_RATING_KEY] - rating[1][self.CURRENT_RATING_KEY]
        else:
            return 0.0

    def get_avg_rating_chart(self):
        rating = self.get_rating()
        # Строим график, только когда есть что показывать
        if len(rating) < 2:
            return None

        # Берем последние 7 дней
        rating = rating[0:self.CHART_MAX_RATINGS]

        data_list = []

        # Считаем минимальное значение графика
        min_rating = 5.0
        # Считаем максимальное значение графика
        max_rating = 0.0
        for item in rating:
            avg_rating = item[self.AVERAGE_RATING_KEY]
            if min_rating > avg_rating:
                min_rating = avg_rating

            if max_rating < avg_rating:
                max_rating = avg_rating

            data_list.insert(0, str(float("{0:.6f}".format(avg_rating))))

        placeholders = {
            'data': ",".join(data_list),
            'axis_start': float("{0:.3f}".format(min_rating - 0.001)),
            'axis_end': float("{0:.3f}".format(max_rating + 0.001)),
        }

        return self.CHART_URL % placeholders
