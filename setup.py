from setuptools import setup, find_packages

setup(name='slack_rating_reporter',
      version='0.2',
      description='Python Distribution Utilities',
      author='Topface Mobile',
      packages=find_packages(),
      requires=['grab'],
      entry_points={
          'console_scripts': [
              'localist = localist.app:main'
          ]
      }
)