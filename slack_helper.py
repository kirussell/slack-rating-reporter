import json

from grab import Grab


class SlackClient:
    def __init__(self, url):
        self.url = url

    def send_message(self, channel, text, username='SlackClient', icon=None):
        payload = {
            'username': username,
            'channel': channel,
            'text': text,
        }
        if icon:
            payload['icon_url'] = icon
        grab = Grab()
        grab.setup(post=json.dumps(payload))
        grab.go(self.url)
        return grab.response.body
